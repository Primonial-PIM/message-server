Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Threading

Imports TCPServer_Common
Imports RenaissanceGlobals

Module MessageHandlers

  '
  ' Wrapper for the TCPServerClass Message handler routines.
  '

  Friend Sub Process_UpdateMessage(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader, ByRef newTcpConnClass As TcpConnClass, ByRef ConnColl As Collection)
    Dim AppName As String
    Dim NewServerMessage As TCPServerHeader
    Dim ObjectArrayList As ArrayList
    Dim ObjectCount As Integer

    ' Convert Client Message to Server Message

    Try
      AppName = pMessageHeader.Application.ToString
    Catch ex As Exception
      AppName = "<Unknown Application>"
    End Try
    If (Not (AppName Is Nothing)) AndAlso (AppName.Length <= 0) Then
      AppName = "<Unknown Application>"
    End If

    NewServerMessage = New TCPServerHeader(TCP_ServerMessageType.Server_ApplicationUpdateMessage, pMessageHeader.Application, pMessageHeader.InstanceName, pMessageHeader.MessageString, 0, 0, pMessageHeader.PathTravelled, pMessageHeader.ReturnPath)
    NewServerMessage.InstanceName = pMessageHeader.InstanceName.ToUpper  ' Add to the New() function above in due course.
    NewServerMessage.FollowingMessageCount = pMessageHeader.FollowingMessageCount
    NewServerMessage.FollowingObjectType = pMessageHeader.FollowingObjectType

    If NewServerMessage.FollowingMessageCount > 0 Then
      ObjectArrayList = New ArrayList

      For ObjectCount = 1 To NewServerMessage.FollowingMessageCount
        Try
          ObjectArrayList.Add(SerializeFormatter.Deserialize(newTcpConnClass.Stream))
        Catch ex As Exception
        End Try
      Next

      NewServerMessage.FollowingMessageCount = ObjectArrayList.Count
    Else
      ObjectArrayList = Nothing
    End If

    ' Pass the message on to the Appropriate Application Type and to other Servers, except to the originating connection

    Dim PropogateConnClass As TcpConnClass
    For Each PropogateConnClass In ConnColl
      If ((PropogateConnClass.ApplicationType And (pMessageHeader.Application Or FCP_Application.MessageServer)) <> FCP_Application.None) AndAlso (Not (PropogateConnClass Is newTcpConnClass)) AndAlso (PropogateConnClass.InstanceName = pMessageHeader.InstanceName) Then
        Try
          If PostToMessageServer(PropogateConnClass, NewServerMessage) = False Then
            PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Error Propagating " & AppName & " Update Message.")
          Else
            If (Not (ObjectArrayList Is Nothing)) AndAlso (ObjectArrayList.Count > 0) Then
              For ObjectCount = 0 To (ObjectArrayList.Count - 1)
                PostToMessageServer(PropogateConnClass, ObjectArrayList(ObjectCount))
              Next
            End If

            PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Sent " & AppName & " Update Message.")
          End If
        Catch ex As Exception
          PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Error Propagating " & AppName & " Update Message. " & ex.Message)
        End Try

      End If
    Next

    ' Post acknowledgement to the originating connection

    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK)

  End Sub

  Friend Sub Process_StatusMessage(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader, ByRef newTcpConnClass As TcpConnClass, ByRef ConnColl As Collection)
    ' ********************************************************************
    ' Just forwards given message without any processing.
    ' Unlike the usual Update messages that convert 'Client' into 'Server' messages 
    '(Which usually makes no difference as generally the Client & server message type enumerations are set to the same value (To avoid problems if the conversion fails to happen.))
    ' ********************************************************************

    Dim AppName As String
    Dim ObjectArrayList As ArrayList
    Dim ObjectCount As Integer

    ' Convert Client Message to Server Message

    Try
      AppName = pMessageHeader.Application.ToString
    Catch ex As Exception
      AppName = "<Unknown Application>"
    End Try
    If (Not (AppName Is Nothing)) AndAlso (AppName.Length <= 0) Then
      AppName = "<Unknown Application>"
    End If

    If pMessageHeader.FollowingMessageCount > 0 Then
      ObjectArrayList = New ArrayList

      For ObjectCount = 1 To pMessageHeader.FollowingMessageCount
        Try
          ObjectArrayList.Add(SerializeFormatter.Deserialize(newTcpConnClass.Stream))
        Catch ex As Exception
        End Try
      Next

      pMessageHeader.FollowingMessageCount = ObjectArrayList.Count
    Else
      ObjectArrayList = Nothing
    End If

    ' Pass the message on to the Appropriate Application Type and to other Servers, except to the originating connection

    Dim PropogateConnClass As TcpConnClass
    For Each PropogateConnClass In ConnColl
      If ((PropogateConnClass.ApplicationType And (pMessageHeader.Application Or FCP_Application.MessageServer)) <> FCP_Application.None) AndAlso (Not (PropogateConnClass Is newTcpConnClass)) AndAlso (PropogateConnClass.InstanceName = pMessageHeader.InstanceName) Then
        Try
          If PostToMessageServer(PropogateConnClass, pMessageHeader) = False Then
            PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Error Propagating " & AppName & " Update Message.")
          Else
            If (Not (ObjectArrayList Is Nothing)) AndAlso (ObjectArrayList.Count > 0) Then
              For ObjectCount = 0 To (ObjectArrayList.Count - 1)
                PostToMessageServer(PropogateConnClass, ObjectArrayList(ObjectCount))
              Next
            End If

            PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Sent " & AppName & " Update Message.")
          End If
        Catch ex As Exception
          PostDebugMessage(DS, "(" & PropogateConnClass.ConnectionID.ToString & ") Error Propagating " & AppName & " Update Message. " & ex.Message)
        End Try

      End If
    Next

    ' Post acknowledgement to the originating connection

    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK)

  End Sub


  Friend Sub Process_GenericDataServerRequest(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader, ByRef ClientConnection As TcpConnClass, ByRef ConnColl As Collection)

    ' Allocate to an available server

    Dim ServerMessage As TCPServerHeader
    Dim ServerMessageType As TCP_ServerMessageType
    Dim ServerApplicationType As FCP_Application
    Dim ServerApplicationName As String
    Dim ServerInstanceName As String

    Select Case pMessageHeader.MessageType
      Case TCP_ServerMessageType.Client_VeniceDataRequest
        ServerMessageType = TCP_ServerMessageType.Server_VeniceDataRequest
        ServerApplicationType = FCP_Application.VeniceDataServer
        ServerApplicationName = "Venice"
        ServerInstanceName = pMessageHeader.InstanceName.ToUpper

      Case TCP_ServerMessageType.Client_NaplesDataRequest
        ServerMessageType = TCP_ServerMessageType.Server_NaplesDataRequest
        ServerApplicationType = FCP_Application.NaplesDataServer
        ServerApplicationName = "BBG"
        ServerInstanceName = pMessageHeader.InstanceName.ToUpper

      Case Else
        ' Unrecognised Request type

        Dim MessageType As String

        Try
          MessageType = System.Enum.GetName(GetType(TCP_ServerMessageType), pMessageHeader.MessageType)
        Catch ex As Exception
          MessageType = "<Unknown Message Type>"
        End Try

        Dim ErrMessageHeader As New TCPServerHeader(TCP_ServerMessageType.Server_MessageRejected, pMessageHeader.Application, pMessageHeader.InstanceName, "Sever does not handle Data request type : " & MessageType, 0, 0)

        Call PostToMessageServer(ClientConnection, ErrMessageHeader)
        Call PostDebugMessage(DS, ClientConnection, ErrMessageHeader)

        Exit Sub
    End Select

    ' Create Server Request Header.
    ' Insert the Client GUID in the Request message so that the reply can be targeted.

    ServerMessage = New TCPServerHeader( _
      ServerMessageType, _
      pMessageHeader.Application, _
      ServerInstanceName, _
      pMessageHeader.MessageString, _
      pMessageHeader.FollowingMessageCount, _
      pMessageHeader.FollowingObjectType, _
      pMessageHeader.PathTravelled, pMessageHeader.ReturnPath)

    ' Select a Data server and relay the message

    Dim ServerCount As Integer
    Dim PostToNextServer As Boolean
    Dim ServerConnectionToUse As TcpConnClass

    Dim thisConnection As TcpConnClass
    thisConnection = Nothing

    ServerCount = 0
    PostToNextServer = False
    ServerConnectionToUse = Nothing

    ' OK, What we want to do is to either post this message back along it's return path or 
    ' Post it to a new Data Server or failing that post it to other message servers.

    If (ServerMessage.ReturnPath.Count > 0) Then
      ' *******************************
      ' Check the return Path
      ' *******************************

      Dim ReturnPathIndex As Integer
      Dim RemoveIndex As Integer

      For ReturnPathIndex = (ServerMessage.ReturnPath.Count - 1) To 0 Step -1
        For Each thisConnection In ConnColl
          If thisConnection.GUID.Equals(CType(ServerMessage.ReturnPath(ReturnPathIndex), Guid)) Then

            ServerConnectionToUse = thisConnection

            For RemoveIndex = (ServerMessage.ReturnPath.Count - 1) To ReturnPathIndex Step -1
              ServerMessage.ReturnPath.RemoveAt(RemoveIndex)
            Next
            Exit For
          End If
        Next

        If Not (ServerConnectionToUse Is Nothing) Then
          Exit For
        End If
      Next

    End If

    If (ServerConnectionToUse Is Nothing) Then
      ' ****************************************************
      ' Choose a new Data Server, excluding Message Servers.
      ' ****************************************************

      For Each thisConnection In ConnColl

        ' If this is not a Server...
        If (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.None Then

          ' But it is of the required Application Server Type and Instance Name...
          If ((thisConnection.ApplicationType And ServerApplicationType) = ServerApplicationType) AndAlso (thisConnection.InstanceName = ServerInstanceName) Then

            ServerCount += 1

            If (PostToNextServer = True) Then
              ServerConnectionToUse = thisConnection
              Exit For
            End If

            If thisConnection.ServerTaskFlag = True Then
              thisConnection.ServerTaskFlag = False
              PostToNextServer = True
            End If
          End If
        End If
      Next

      ' If no Connection has been settled on but Application Servers do exist, look for the first 
      ' available Application Server, Ignoring MessageServers as before...

      ' Huh ? (NPP Sep 2013) Check this logic if we ever actually use Data Servers !!!!

      If ((thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.None) Then
        If (ServerCount > 0) And (ServerConnectionToUse Is Nothing) Then
          For Each thisConnection In ConnColl
            If ((thisConnection.ApplicationType And ServerApplicationType) = ServerApplicationType) AndAlso (thisConnection.InstanceName = ServerInstanceName) Then

              ServerConnectionToUse = thisConnection
              Exit For
            End If
          Next
        End If
      End If
    End If

    If (ServerConnectionToUse Is Nothing) Then
      ' ***********************************************************************************
      ' Choose a new Data Server, Including Message Servers.
      ' This step is different to the next step in that it gives precedence to servers that 
      ' have registered as a given application type over Servers that Have not.
      ' ***********************************************************************************

      ServerCount = 0
      For Each thisConnection In ConnColl

        ' If this connection is not in the PathTravelled, then...
        If (GUIDinPath(ServerMessage.PathTravelled, thisConnection.GUID) = False) Then

          ' If this is an Application Server (or a Message Server pretending) then
          If ((thisConnection.ApplicationType And ServerApplicationType) = ServerApplicationType) AndAlso (thisConnection.InstanceName = ServerInstanceName) Then
            ServerCount += 1

            If (PostToNextServer = True) Then
              ServerConnectionToUse = thisConnection
              Exit For
            End If

            If thisConnection.ServerTaskFlag = True Then
              thisConnection.ServerTaskFlag = False
              PostToNextServer = True
            End If
          End If
        End If
      Next


      ' If no Connection has been settled on but Application (or Message proxy) Servers do exist, 
      ' look for the first available Application Server, using the First Plain Message Server as a last resort.

      ' so, If no destination has been resolved...
      If (ServerCount > 0) And (ServerConnectionToUse Is Nothing) Then

        ' For Each Connection...
        For Each thisConnection In ConnColl

          ' If it is not already in the path...
          If (GUIDinPath(ServerMessage.PathTravelled, thisConnection.GUID) = False) Then

            ' And it is registered as the correct App Server type
            If ((thisConnection.ApplicationType And ServerApplicationType) = ServerApplicationType) AndAlso (thisConnection.InstanceName = ServerInstanceName) Then

              ServerConnectionToUse = thisConnection
              Exit For
            ElseIf (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.MessageServer Then
              ' Otherwise use any available Message Server as a last resort.

              If (ServerConnectionToUse Is Nothing) Then
                ServerConnectionToUse = thisConnection
              End If
            End If
          End If
        Next
      End If
    End If

    ' **************************************************************
    ' No Data Servers are registered and No Message Servers will do.
    ' **************************************************************
    If (ServerCount <= 0) OrElse (ServerConnectionToUse Is Nothing) Then
      Try
        ServerMessage.MessageType = TCP_ServerMessageType.Client_MessageRejected
        ServerMessage.MessageString = "No " & ServerApplicationName & " Data Server is Registered (Instance `" & ServerInstanceName & "`)"
        ServerMessage.Application = ServerApplicationType
        ServerMessage.ReturnPath = ServerMessage.PathTravelled
        ServerMessage.PathTravelled = New ArrayList

        ' Take the Client GUID of the top of the Return path.
        If (ServerMessage.ReturnPath.Count > 0) Then
          If ClientConnection.GUID.Equals(CType(ServerMessage.ReturnPath(ServerMessage.ReturnPath.Count - 1), Guid)) Then
            ServerMessage.ReturnPath.RemoveAt(ServerMessage.ReturnPath.Count - 1)
          End If
        End If

        ' Return Message Stream 
        Call PostToMessageServer(ClientConnection, ServerMessage)
        Call ForwardFollowingMessages(ServerMessage, ClientConnection, ClientConnection)

        Call PostDebugMessage(DS, ClientConnection, ServerMessage)

      Catch ex As Exception
      End Try

    Else
      ' **************************************************************
      ' SetUsage Flag and post message(s)
      ' **************************************************************

      ServerConnectionToUse.ServerTaskFlag = True

      Try
        Call PostToMessageServer(ServerConnectionToUse, ServerMessage)
        Call ForwardFollowingMessages(ServerMessage, ClientConnection, ServerConnectionToUse)

        Call PostDebugMessage(DS, ClientConnection, ServerMessage)
      Catch ex As Exception
      End Try

    End If

  End Sub


  Friend Sub Process_GenericDataServerAnswer(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader, ByRef newTcpConnClass As TcpConnClass, ByRef ConnColl As Collection)

    Dim ServerMessage As TCPServerHeader
    Dim ServerMessageType As TCP_ServerMessageType
    Dim ServerApplicationType As FCP_Application

    ServerMessageType = pMessageHeader.MessageType
    ServerApplicationType = pMessageHeader.Application

    Select Case pMessageHeader.MessageType
      Case TCP_ServerMessageType.Client_VeniceDataAnswer, TCP_ServerMessageType.Server_VeniceDataAnswer
        ServerMessageType = TCP_ServerMessageType.Server_VeniceDataAnswer
        ServerApplicationType = FCP_Application.VeniceDataServer

      Case TCP_ServerMessageType.Client_NaplesDataAnswer, TCP_ServerMessageType.Server_NaplesDataAnswer
        ServerMessageType = TCP_ServerMessageType.Server_NaplesDataAnswer
        ServerApplicationType = FCP_Application.NaplesDataServer

      Case TCP_ServerMessageType.Client_KansasStatusAnswer, TCP_ServerMessageType.Server_KansasStatusAnswer
        ServerMessageType = TCP_ServerMessageType.Server_KansasStatusAnswer
        ServerApplicationType = FCP_Application.Kansas
    End Select

    ServerMessage = New TCPServerHeader( _
      ServerMessageType, _
      ServerApplicationType, _
      pMessageHeader.InstanceName, _
      pMessageHeader.MessageString, _
      pMessageHeader.FollowingMessageCount, _
      pMessageHeader.FollowingObjectType, _
      pMessageHeader.PathTravelled, pMessageHeader.ReturnPath)

    PassMessageAlongReturnPath(DS, ServerMessage, newTcpConnClass)

    ' Ahhhhhh, Done....

    Call PostDebugMessage(DS, newTcpConnClass, ServerMessage)

  End Sub


  Friend Sub ForwardFollowingMessages(ByRef pMessageHeader As TCPServerHeader, ByRef SourceConnection As TcpConnClass, ByRef TargetConnection As TcpConnClass)

    If (pMessageHeader.FollowingMessageCount <= 0) Then
      Exit Sub
    End If

    Dim FollowingMessageCounter As Integer
    Dim ObjectToForward As Object

    For FollowingMessageCounter = 1 To pMessageHeader.FollowingMessageCount
      Try

        ' Check that there is data available.
        ' If not, then wait for it for a little while, then exit.
        If (SourceConnection.Stream.DataAvailable = False) Then
          Dim DataCounter As Integer

          DataCounter = 0
          For DataCounter = 1 To 10
            If (SourceConnection.Stream.DataAvailable = False) Then
              Thread.Sleep(25)
            End If
          Next
          If (SourceConnection.Stream.DataAvailable = False) Then
            Exit Sub
          End If
        End If

        ' Forward the object.
        ObjectToForward = SerializeFormatter.Deserialize(SourceConnection.Stream)

        If Not (TargetConnection Is Nothing) Then
          Call PostToMessageServer(TargetConnection, ObjectToForward)
        End If

      Catch IOex As IOException

        Dim ErrorMessage As String
        ErrorMessage = IOex.Message

      Catch ex As Exception

        Call PostToMessageServer(TargetConnection, New TCPServerHeader(TCP_ServerMessageType.Server_ErrorMessage, FCP_Application.None, pMessageHeader.InstanceName, ex.Message, 0, 0))
      End Try
    Next


  End Sub

  Friend Sub BinFollowingMessages(ByRef pMessageHeader As TCPServerHeader, ByRef SourceConnection As TcpConnClass)

    If (pMessageHeader.FollowingMessageCount > 0) Then
      Call ForwardFollowingMessages(pMessageHeader, SourceConnection, Nothing)
    End If

  End Sub

  Friend Sub PassMessageAlongReturnPath(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader, ByRef SourceConnection As TcpConnClass)

    Dim thisConnection As TcpConnClass
    Dim ServerConnectionToUse As TcpConnClass
    Dim ReturnPathIndex As Integer
    Dim RemoveIndex As Integer

    ServerConnectionToUse = Nothing

    If (pMessageHeader.ReturnPath.Count > 0) Then
      For ReturnPathIndex = (pMessageHeader.ReturnPath.Count - 1) To 0 Step -1
        For Each thisConnection In DS.ConnColl
          If thisConnection.GUID.Equals(CType(pMessageHeader.ReturnPath(ReturnPathIndex), Guid)) Then

            ServerConnectionToUse = thisConnection

            Try
              For RemoveIndex = (pMessageHeader.ReturnPath.Count - 1) To ReturnPathIndex Step -1
                pMessageHeader.ReturnPath.RemoveAt(RemoveIndex)
              Next
            Catch ex As Exception
            End Try

            Exit For
          End If
        Next

        If Not (ServerConnectionToUse Is Nothing) Then
          Exit For
        End If
      Next
    End If

    If Not (ServerConnectionToUse Is Nothing) Then
      ' Pass message set along

      Call PostToMessageServer(ServerConnectionToUse, pMessageHeader)
      Call ForwardFollowingMessages(pMessageHeader, SourceConnection, ServerConnectionToUse)
    Else

      ' If there was no Return Path, or the Connection does not exist, then....
      ' Return the message along the PathTravelled, Set message type to 'Rejected'.

      Try
        pMessageHeader.MessageType = TCP_ServerMessageType.Server_MessageRejected

        pMessageHeader.ReturnPath = pMessageHeader.PathTravelled
        pMessageHeader.PathTravelled = New ArrayList
        If (pMessageHeader.ReturnPath.Count > 0) Then
          pMessageHeader.ReturnPath.RemoveAt(pMessageHeader.ReturnPath.Count - 1)
        End If
      Catch ex As Exception
      End Try

      Call PostToMessageServer(SourceConnection, pMessageHeader)
      Call ForwardFollowingMessages(pMessageHeader, SourceConnection, SourceConnection)
    End If

  End Sub


  Friend Function GUIDinPath(ByRef GUIDPath As ArrayList, ByVal pGuid As Guid) As Boolean

    Dim PathIndex As Integer

    If (GUIDPath.Count > 0) Then
      For PathIndex = (GUIDPath.Count - 1) To 0 Step -1
        If pGuid.Equals(CType(GUIDPath(PathIndex), Guid)) Then
          Return True
        End If
      Next
    End If

    Return False

  End Function

End Module
