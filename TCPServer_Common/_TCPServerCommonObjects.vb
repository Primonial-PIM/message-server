Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Collections
Imports RenaissanceGlobals

Public Class NetworkFunctions

  Public Shared Function GetServerAddress(ByVal MessageServerName As String) As System.Net.IPAddress
    Try
      Dim AddressArray() As System.Net.IPAddress = Dns.GetHostEntry(MessageServerName).AddressList

      For Each thisAddress As System.Net.IPAddress In AddressArray
        If (thisAddress.AddressFamily = TCPServerGlobals.RENAISSANCE_NETWORK_FAMILY) Then
          Return thisAddress
          Exit Function
        End If
      Next

    Catch ex As Exception
    End Try

    Return System.Net.IPAddress.None

  End Function

End Class

' Essential TCP Server Global information

Public Class TCPServerGlobals

  Public Const FCP_ServerPort As Integer = 9768
  Public Const FCP_CLIENT_WAKEUP_TIMER As Integer = 60 ' secs
  Public Const FCP_CLIENT_RENEW_CONNECTION_TIMER As Integer = 200 ' secs
  Public Const FCP_Server_ReceiveTimeout As Integer = 1000 ' ms
  Public Const FCP_Server_SendTimeout As Integer = 1000

  ' Network Address Family to use (Partial implimentation - To complete NPP 8 Nov 2012)

  Public Const RENAISSANCE_NETWORK_FAMILY As System.Net.Sockets.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork

End Class

Public Class TcpConnClass
  Public Client As TcpClient
  Public Stream As NetworkStream
  Public bReader As BinaryReader
  Public bWriter As BinaryWriter
  Public ConnectionID As Int32
  Public WakeupTime As Date
  Public GUID As GUID
  Public ActiveConnection As Boolean
  Public DeleteMe As Boolean
	Public RequestConnectString As String	' Usually the User name
	Public ClientIPAddress As System.Net.IPAddress

  Public InstanceName As String
  Public ApplicationType As FCP_Application
  Public ServerTaskFlag As Boolean

  Public Sub New()
    Me.ActiveConnection = False
		Me.DeleteMe = False
    Me.RequestConnectString = ""
    Me.InstanceName = ""
		Me.ClientIPAddress = Nothing
  End Sub
End Class

' *****************************************************************
' Standard Message Header Object
'
' *****************************************************************

<Serializable()> Public Class TCPServerHeader

  Public MessageType As TCP_ServerMessageType
  Public Application As FCP_Application
  Public InstanceName As String = ""
  Public MessageString As String
  Public FollowingMessageCount As Integer
  Public FollowingObjectType As Integer

  Public PathTravelled As ArrayList
  Public ReturnPath As ArrayList

  'Public RequestingGUID As Guid
  'Public RequestingMessage As TCPServerHeader

  Private Sub New()

  End Sub

  'Public Sub New()
  '  Me.New(TCP_ServerMessageType.None, FCP_Application.None, "", 0, 0)
  'End Sub

  'Public Sub New(ByVal pMessageType As TCP_ServerMessageType)
  '  Me.New(pMessageType, FCP_Application.None, "", 0, 0)
  'End Sub

  'Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pApplication As FCP_Application)
  '  Me.New(pMessageType, pApplication, "", 0, 0)
  'End Sub

  'Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pMessageString As String)
  '  Me.New(pMessageType, FCP_Application.None, pMessageString, 0, 0)
  'End Sub

  'Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pApplication As FCP_Application, ByVal pMessageString As String)
  '  Me.New(pMessageType, pApplication, pMessageString, 0, 0)
  'End Sub

  'Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pApplication As FCP_Application, ByVal pMessageString As String, ByVal pRequestingMessage As TCPServerHeader, ByRef pPathTravelled As ArrayList, ByRef pReturnPath As ArrayList)
  '  Me.New(pMessageType, pApplication, pMessageString, 0, 0, pPathTravelled, pReturnPath)
  'End Sub

  Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pApplication As FCP_Application, ByVal pInstanceName As String, ByVal pMessageString As String, ByVal pFollowingMessageCount As Integer, ByVal pFollowingObjectType As Integer)
    Me.New(pMessageType, pApplication, pInstanceName, pMessageString, pFollowingMessageCount, pFollowingObjectType, New ArrayList, New ArrayList)
  End Sub

  Public Sub New(ByVal pMessageType As TCP_ServerMessageType, ByVal pApplication As FCP_Application, ByVal pInstanceName As String, ByVal pMessageString As String, ByVal pFollowingMessageCount As Integer, ByVal pFollowingObjectType As Integer, ByRef pPathTravelled As ArrayList, ByRef pReturnPath As ArrayList)
    Me.MessageType = pMessageType
    Me.Application = pApplication
    Me.InstanceName = pInstanceName.ToUpper
    Me.MessageString = pMessageString
    Me.FollowingMessageCount = pFollowingMessageCount
    Me.FollowingObjectType = pFollowingObjectType
    'Me.RequestingGUID = pRequestingGUID
    'Me.RequestingMessage = pRequestingMessage
    Me.PathTravelled = pPathTravelled
    Me.ReturnPath = pReturnPath
  End Sub

End Class


' *****************************************************************
'
'
' *****************************************************************
<Serializable()> Public Class TCPServerObjectWrapper
  Public ObjectType As String
  Public ObjectFormat As String
  Public ObjectData As Byte()
End Class


' *****************************************************************
'
'
' *****************************************************************
Public Class MessageReceivedEventArgs
  Inherits EventArgs

  Private _MessageReceivedID As Guid

  Private _HeaderMessage As TCPServerHeader
  Private _FollowingObjects As ArrayList
	Private Const HeaderMessageSeparationCharacters As String = ",|�"

	Private Sub New()
		' Disallow.
	End Sub

  Public Sub New(ByRef pHeaderMessage As TCPServerHeader, ByRef pFollowingObjects As ArrayList)
    _MessageReceivedID = Guid.NewGuid
    _HeaderMessage = pHeaderMessage
    _FollowingObjects = pFollowingObjects
  End Sub

  Public ReadOnly Property MessageReceivedID() As Guid
    Get
      Return _MessageReceivedID
    End Get
  End Property

  Public ReadOnly Property HeaderMessage() As TCPServerHeader
    Get
      Return _HeaderMessage
    End Get
  End Property

  Public ReadOnly Property FollowingObjects() As ArrayList
    Get
      Return _FollowingObjects
    End Get
  End Property

	' Original
	'Public ReadOnly Property NaplesDataAnswer() As NaplesDataAnswer
	'  Get
	'    If (_FollowingObjects.Count <= 0) Then
	'      Return Nothing
	'    Else
	'      Try
	'        If (_FollowingObjects(0).GetType Is GetType(NaplesDataAnswer)) Then
	'          Return CType(_FollowingObjects(0), NaplesDataAnswer)
	'        Else
	'          Return Nothing
	'        End If
	'      Catch ex As Exception
	'        Return Nothing
	'      End Try
	'    End If
	'  End Get
	'End Property
	'
	' Fudge so I don't have to include Naples globals at the moment.
	' It uses different BBg libraries to Kansas.
	' Must update Naples one day, if we want to.....
	Public ReadOnly Property NaplesDataAnswer(ByVal NaplesDataAnswerType As Type) As Object
		Get
			If (_FollowingObjects.Count <= 0) Then
				Return Nothing
			Else
				Try
					If (_FollowingObjects(0).GetType Is NaplesDataAnswerType) Then
						Return _FollowingObjects(0)
					Else
						Return Nothing
					End If
				Catch ex As Exception
					Return Nothing
				End Try
			End If
		End Get
	End Property

  Public ReadOnly Property UpdateMessage(ByVal pApplicationType As FCP_Application) As Integer
    ' ****************************************************************************************
    ' Return the RenaissanceChangeID embedded in the MessageHeader Message string.
    ' 
    ' The Message String format for an update message is :
    ' [[ ChangeIDNumber | ChangeIDName] [, UpdateDetailString]]
    ' That is, the Change Identifier specified as either a number or an Enumeration name (usually
    ' stored as a name) optionally followed by a seperation character (',', '|' or '�') and an update
    ' Detail. As of writing (Apr 2008) this is unused, but it is my intention that it will may contain
    ' the ID for the changed item (specifically implemented for performance reasons to refine the 
    ' data refresh requirements).
    '
    ' ****************************************************************************************

    Get
      Dim RVal As Integer = 0

      If (_HeaderMessage.Application And pApplicationType) <> FCP_Application.None Then

        Try
          If (_HeaderMessage.MessageString.Length > 0) Then
            Dim TempArray() As String

            TempArray = _HeaderMessage.MessageString.Split(HeaderMessageSeparationCharacters.ToCharArray)

            If (TempArray IsNot Nothing) AndAlso (TempArray.Length > 0) Then
              If IsNumeric(TempArray(0)) Then
                Return (CInt(TempArray(0)))
              Else
                If (System.Enum.IsDefined(GetType(RenaissanceChangeID), TempArray(0))) Then
                  Return CInt(System.Enum.Parse(GetType(RenaissanceChangeID), TempArray(0)))
                Else
                  RVal = 0
                End If

                Return CInt(System.Enum.Parse(GetType(RenaissanceChangeID), TempArray(0)))
              End If
            End If
          End If
        Catch ex As Exception
          RVal = 0
        End Try
      End If

      Return RVal
    End Get
  End Property

	Public ReadOnly Property UpdateDetail(ByVal pApplicationType As FCP_Application) As String
		Get
			Dim RVal As String = ""

			Try
				If (_HeaderMessage.Application And pApplicationType) <> FCP_Application.None Then
					If (_HeaderMessage.MessageString.Length > 0) Then
						Dim TempArray() As String

            TempArray = _HeaderMessage.MessageString.Split(HeaderMessageSeparationCharacters.ToCharArray, 2) ' Limit to Two entries (in order to keep Multi-Part Update Strings together).
						If (TempArray IsNot Nothing) AndAlso (TempArray.Length > 1) Then
							RVal = TempArray(1)
						End If
					End If
				End If
			Catch ex As Exception
				RVal = ""
			End Try

			Return RVal
		End Get
	End Property


End Class


' *****************************************************************
'
'
' *****************************************************************
Public Class MessageFolder
  Private _MessageFolderID As Guid

  Private _HeaderMessage As TCPServerHeader
  Private _FollowingObjects As ArrayList

  Public Sub New()
		_MessageFolderID = Guid.NewGuid
		_HeaderMessage = Nothing
		_FollowingObjects = Nothing
	End Sub

	Public Sub New(ByVal pHeaderMessage As TCPServerHeader, ByVal pFollowingObjects As ArrayList)
		_MessageFolderID = Guid.NewGuid
		_HeaderMessage = pHeaderMessage
		_FollowingObjects = pFollowingObjects
	End Sub

	Public Sub New(ByVal pHeaderMessage As TCPServerHeader, ByVal pFollowingObject1 As Object)
		_MessageFolderID = Guid.NewGuid
		_HeaderMessage = pHeaderMessage

		If (_HeaderMessage Is Nothing) Then
			_FollowingObjects = Nothing
		Else
			If (pFollowingObject1 Is Nothing) Then
				_HeaderMessage.FollowingMessageCount = 0
				_FollowingObjects = Nothing
			Else
				_HeaderMessage.FollowingMessageCount = 1
				_FollowingObjects = New ArrayList
				_FollowingObjects.Add(pFollowingObject1)
			End If
		End If
	End Sub

	Public Sub New(ByVal pHeaderMessage As TCPServerHeader, ByVal pFollowingObject1 As Object, ByVal pFollowingObject2 As Object)
		_MessageFolderID = Guid.NewGuid
		_HeaderMessage = pHeaderMessage
		_FollowingObjects = New ArrayList

		If (Not (pFollowingObject1 Is Nothing)) Then
			_FollowingObjects.Add(pFollowingObject1)
		End If
		If (Not (pFollowingObject2 Is Nothing)) Then
			_FollowingObjects.Add(pFollowingObject2)
		End If

		If (Not (_HeaderMessage Is Nothing)) Then
			_HeaderMessage.FollowingMessageCount = _FollowingObjects.Count
		Else
			_FollowingObjects = Nothing
		End If
	End Sub

  Public Sub New(ByRef pHeaderMessage As TCPServerHeader)
    _MessageFolderID = Guid.NewGuid
    _HeaderMessage = pHeaderMessage
    _FollowingObjects = Nothing
  End Sub

  Public ReadOnly Property MessageFolderID() As Guid
    Get
      Return _MessageFolderID
    End Get
  End Property

  Public ReadOnly Property HeaderMessage() As TCPServerHeader
    Get
      Return _HeaderMessage
    End Get
  End Property

  Public ReadOnly Property FollowingObjects() As ArrayList
    Get
      Return _FollowingObjects
    End Get
  End Property

	'Public ReadOnly Property NaplesDataAnswer() As NaplesDataAnswer
	'  Get
	'    If (_FollowingObjects.Count <= 0) Then
	'      Return Nothing
	'    Else
	'      Try
	'        If (_FollowingObjects(0).GetType Is GetType(NaplesDataAnswer)) Then
	'          Return CType(_FollowingObjects(0), NaplesDataAnswer)
	'        Else
	'          Return Nothing
	'        End If
	'      Catch ex As Exception
	'        Return Nothing
	'      End Try
	'    End If
	'  End Get
	'End Property
	' Fudge so I don't have to include Naples globals at the moment.
	' It uses different BBg libraries to Kansas.
	' Must update Naples one day, if we want to.....
	Public ReadOnly Property NaplesDataAnswer(ByVal NaplesDataAnswerType As Type) As Object
		Get
			If (_FollowingObjects.Count <= 0) Then
				Return Nothing
			Else
				Try
					If (_FollowingObjects(0).GetType Is NaplesDataAnswerType) Then
						Return _FollowingObjects(0)
					Else
						Return Nothing
					End If
				Catch ex As Exception
					Return Nothing
				End Try
			End If
		End Get
	End Property


  Public ReadOnly Property VeniceUpdateClass() As RenaissanceUpdateClass
    Get
      If (_FollowingObjects.Count <= 0) Then
        Return Nothing
      Else
        Try
          If (_FollowingObjects(0).GetType Is GetType(RenaissanceUpdateClass)) Then
            Return CType(_FollowingObjects(0), RenaissanceUpdateClass)
          Else
            Return Nothing
          End If
        Catch ex As Exception
          Return Nothing
        End Try
      End If
    End Get
  End Property

  Public ReadOnly Property UpdateMessage(ByVal pApplicationType As FCP_Application) As Integer
    Get
      If (_HeaderMessage.Application And pApplicationType) <> FCP_Application.None Then
        Try
          If IsNumeric(_HeaderMessage.MessageString) Then
            Return (CInt(_HeaderMessage.MessageString))
          Else
            Return 0
          End If
        Catch ex As Exception
          Return 0
        End Try
      Else
        Return 0
      End If
    End Get
  End Property


End Class


' *****************************************************************
'
'
' *****************************************************************
Public Enum TCP_ServerMessageType
  None = 0

  Server_AcknowledgeOK = 1
  Client_AcknowledgeOK = 1
  Server_Cancel = 2
  Client_Cancel = 2
  Server_Disconnect = 3
  Client_Disconnect = 3
  Server_Touchbase = 4
  Client_Touchbase = 4
  Server_MessageRejected = 5
  Client_MessageRejected = 5
  Server_ErrorMessage = 6

  Server_RequestConnect = 10
  Client_RequestConnect = 10

  Client_RegisterApplication = 20
  Client_UnRegisterApplication = 21

  Server_FindPath = 31
  Client_FindPath = 31
  Server_PathFound = 33
  Client_PathFound = 33

  Server_DebugMessage = 100
  Server_GUID_Message = 101
  Client_GetGUID = 102
  Client_SetGUID = 103

  ' Generic Update Message
  Client_ApplicationUpdateMessage = 5000
  Server_ApplicationUpdateMessage = 5001

  ' BBG Data Request
  Server_NaplesDataRequest = 10002
  Client_NaplesDataRequest = 10002
  Server_NaplesDataAnswer = 10003
  Client_NaplesDataAnswer = 10003

  ' Venice Messages
  Server_VeniceDataRequest = 20001
  Client_VeniceDataRequest = 20001
  Server_VeniceDataAnswer = 20002
  Client_VeniceDataAnswer = 20002

  ' Genoa Messages


  ' Modena Messages


  ' Pisa Messages


  ' Florence Messages


  ' Milan Messages


  ' Kansas Messages

  Server_KansasStatusRequest = 21001
  Client_KansasStatusRequest = 21001
  Server_KansasStatusAnswer = 21002
  Client_KansasStatusAnswer = 21002

  Client_KansasStatusMessage = 21003

  ' Debug Messages
  Client_Debug_GetMessageServerConnections = 40001


End Enum

' *****************************************************************
'
'
' *****************************************************************
<FlagsAttribute()> Public Enum FCP_Application As Long ' 64 bit
  None = 0
  Debug = 1
  Venice = CLng(2 ^ 1)
  Milan = CLng(2 ^ 2)
  Genoa = CLng(2 ^ 3)
  Modena = CLng(2 ^ 4)
  Pisa = CLng(2 ^ 5)
  Florence = CLng(2 ^ 6)
  Naples = CLng(2 ^ 7)
  MessageServer = CLng(2 ^ 8)
  Renaissance = CLng(2 ^ 9)
  Reporter = CLng(2 ^ 10)
  Sienna = CLng(2 ^ 11)
  Lucca = CLng(2 ^ 12)
  Kansas = CLng(2 ^ 13)
  Michegan = CLng(2 ^ 14)

  VeniceDataServer = CLng(2 ^ 32)
  VeniceDataClient = CLng(2 ^ 33)
  NaplesDataServer = CLng(2 ^ 34)
  NaplesDataClient = CLng(2 ^ 35)

End Enum

