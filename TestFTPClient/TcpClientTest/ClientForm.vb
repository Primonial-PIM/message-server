Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Windows.Forms
Imports TCPServer_Common
Imports TCPServer_Common.PostMessageUtils
Imports NaplesGlobals
Imports ScreenDecoders.ScreenDecoders
Imports System.Globalization
Imports RenaissanceGlobals

Public Class ClientForm
  Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button_Connect As System.Windows.Forms.Button
  Friend WithEvents TextBox_Server As System.Windows.Forms.TextBox
  Friend WithEvents TextBox_Dialog As System.Windows.Forms.TextBox
  Friend WithEvents Button_Disconnect As System.Windows.Forms.Button
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Text_MessageText As System.Windows.Forms.TextBox
  Friend WithEvents CB_Message As System.Windows.Forms.ComboBox
  Friend WithEvents CB_Application As System.Windows.Forms.ComboBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Button_SendMessage As System.Windows.Forms.Button
  Friend WithEvents CB_DataRequestType As System.Windows.Forms.ComboBox
  Friend WithEvents Button1 As System.Windows.Forms.Button
  Friend WithEvents Button3 As System.Windows.Forms.Button
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents TextBox_Instance As System.Windows.Forms.TextBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Button2 As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.Button_Connect = New System.Windows.Forms.Button
    Me.TextBox_Server = New System.Windows.Forms.TextBox
    Me.TextBox_Dialog = New System.Windows.Forms.TextBox
    Me.Button_Disconnect = New System.Windows.Forms.Button
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.Label1 = New System.Windows.Forms.Label
    Me.Text_MessageText = New System.Windows.Forms.TextBox
    Me.CB_Message = New System.Windows.Forms.ComboBox
    Me.CB_Application = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Button_SendMessage = New System.Windows.Forms.Button
    Me.CB_DataRequestType = New System.Windows.Forms.ComboBox
    Me.Button1 = New System.Windows.Forms.Button
    Me.Button2 = New System.Windows.Forms.Button
    Me.Button3 = New System.Windows.Forms.Button
    Me.Label5 = New System.Windows.Forms.Label
    Me.TextBox_Instance = New System.Windows.Forms.TextBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.SuspendLayout()
    '
    'Button_Connect
    '
    Me.Button_Connect.Location = New System.Drawing.Point(456, 16)
    Me.Button_Connect.Name = "Button_Connect"
    Me.Button_Connect.Size = New System.Drawing.Size(78, 24)
    Me.Button_Connect.TabIndex = 0
    Me.Button_Connect.Text = "Connect"
    '
    'TextBox_Server
    '
    Me.TextBox_Server.Location = New System.Drawing.Point(128, 16)
    Me.TextBox_Server.Name = "TextBox_Server"
    Me.TextBox_Server.Size = New System.Drawing.Size(312, 20)
    Me.TextBox_Server.TabIndex = 1
    '
    'TextBox_Dialog
    '
    Me.TextBox_Dialog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Dialog.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.TextBox_Dialog.Location = New System.Drawing.Point(8, 210)
    Me.TextBox_Dialog.Multiline = True
    Me.TextBox_Dialog.Name = "TextBox_Dialog"
    Me.TextBox_Dialog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_Dialog.Size = New System.Drawing.Size(728, 446)
    Me.TextBox_Dialog.TabIndex = 2
    Me.TextBox_Dialog.Text = "TextBox2"
    '
    'Button_Disconnect
    '
    Me.Button_Disconnect.Location = New System.Drawing.Point(540, 16)
    Me.Button_Disconnect.Name = "Button_Disconnect"
    Me.Button_Disconnect.Size = New System.Drawing.Size(75, 24)
    Me.Button_Disconnect.TabIndex = 4
    Me.Button_Disconnect.Text = "Dis-Connect"
    '
    'Timer1
    '
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(8, 16)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(112, 24)
    Me.Label1.TabIndex = 8
    Me.Label1.Text = "Server"
    '
    'Text_MessageText
    '
    Me.Text_MessageText.Location = New System.Drawing.Point(128, 122)
    Me.Text_MessageText.Name = "Text_MessageText"
    Me.Text_MessageText.Size = New System.Drawing.Size(576, 20)
    Me.Text_MessageText.TabIndex = 9
    '
    'CB_Message
    '
    Me.CB_Message.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.CB_Message.Location = New System.Drawing.Point(128, 68)
    Me.CB_Message.Name = "CB_Message"
    Me.CB_Message.Size = New System.Drawing.Size(576, 21)
    Me.CB_Message.Sorted = True
    Me.CB_Message.TabIndex = 10
    '
    'CB_Application
    '
    Me.CB_Application.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.CB_Application.Location = New System.Drawing.Point(128, 95)
    Me.CB_Application.Name = "CB_Application"
    Me.CB_Application.Size = New System.Drawing.Size(576, 21)
    Me.CB_Application.Sorted = True
    Me.CB_Application.TabIndex = 11
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(8, 68)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(112, 24)
    Me.Label2.TabIndex = 12
    Me.Label2.Text = "Message"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(8, 95)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(112, 24)
    Me.Label3.TabIndex = 13
    Me.Label3.Text = "Application"
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(8, 122)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(112, 20)
    Me.Label4.TabIndex = 14
    Me.Label4.Text = "Message Text"
    '
    'Button_SendMessage
    '
    Me.Button_SendMessage.Location = New System.Drawing.Point(128, 175)
    Me.Button_SendMessage.Name = "Button_SendMessage"
    Me.Button_SendMessage.Size = New System.Drawing.Size(240, 24)
    Me.Button_SendMessage.TabIndex = 15
    Me.Button_SendMessage.Text = "Send Message"
    '
    'CB_DataRequestType
    '
    Me.CB_DataRequestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.CB_DataRequestType.Location = New System.Drawing.Point(128, 148)
    Me.CB_DataRequestType.Name = "CB_DataRequestType"
    Me.CB_DataRequestType.Size = New System.Drawing.Size(240, 21)
    Me.CB_DataRequestType.Sorted = True
    Me.CB_DataRequestType.TabIndex = 16
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(657, 16)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(47, 24)
    Me.Button1.TabIndex = 17
    Me.Button1.Text = "Test 1"
    '
    'Button2
    '
    Me.Button2.Location = New System.Drawing.Point(557, 175)
    Me.Button2.Name = "Button2"
    Me.Button2.Size = New System.Drawing.Size(147, 24)
    Me.Button2.TabIndex = 18
    Me.Button2.Text = "Application Terminate"
    '
    'Button3
    '
    Me.Button3.Location = New System.Drawing.Point(389, 175)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(77, 24)
    Me.Button3.TabIndex = 19
    Me.Button3.Text = "Clear Text"
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(10, 151)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(112, 20)
    Me.Label5.TabIndex = 20
    Me.Label5.Text = "DataRequest Type"
    '
    'TextBox_Instance
    '
    Me.TextBox_Instance.Location = New System.Drawing.Point(128, 42)
    Me.TextBox_Instance.Name = "TextBox_Instance"
    Me.TextBox_Instance.Size = New System.Drawing.Size(312, 20)
    Me.TextBox_Instance.TabIndex = 21
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(8, 42)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(112, 24)
    Me.Label6.TabIndex = 22
    Me.Label6.Text = "Instance"
    '
    'ClientForm
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(744, 662)
    Me.Controls.Add(Me.TextBox_Instance)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Button3)
    Me.Controls.Add(Me.Button2)
    Me.Controls.Add(Me.Button1)
    Me.Controls.Add(Me.CB_DataRequestType)
    Me.Controls.Add(Me.Button_SendMessage)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.CB_Application)
    Me.Controls.Add(Me.CB_Message)
    Me.Controls.Add(Me.Text_MessageText)
    Me.Controls.Add(Me.TextBox_Dialog)
    Me.Controls.Add(Me.TextBox_Server)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Button_Disconnect)
    Me.Controls.Add(Me.Button_Connect)
    Me.Name = "ClientForm"
    Me.Text = "ClientForm"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

  Private Stream As NetworkStream
  Private Client As TcpClient
  Dim w As BinaryWriter
  Dim r As BinaryReader

  Dim LastAnswer As NaplesDataAnswer

  Private SerializeFormatter As New BinaryFormatter

  Private Sub ClientForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.Timer1.Interval = 100
    Me.Timer1.Start()

    Me.TextBox_Server.Text = System.Environment.MachineName

    Me.CB_Message.Items.Clear()
    Me.CB_Message.Items.AddRange(System.Enum.GetNames(GetType(TCP_ServerMessageType)))

    Me.CB_Application.Items.Clear()
    Me.CB_Application.Items.AddRange(System.Enum.GetNames(GetType(FCP_Application)))

    Me.CB_DataRequestType.Items.Clear()
    Me.CB_DataRequestType.Items.AddRange(System.Enum.GetNames(GetType(NaplesGlobals.DS_RequestType)))
  End Sub

  Private Sub ClientForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Call Button_Disconnect_Click(Me, New System.EventArgs)

  End Sub

  Private Sub Button_Connect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Connect.Click
    Client = New TcpClient
		Client.NoDelay = True

    If (Stream Is Nothing) Then
      'Client.Connect(IPAddress.Parse("127.0.0.1"), 9768)

			Dim NewIPAddress As IPAddress = Nothing
			Dim IPAddressArray() As IPAddress

			Try
				IPAddressArray = Dns.GetHostEntry(Me.TextBox_Server.Text).AddressList

				For Each NewIPAddress In IPAddressArray
					Try

						Me.TextBox_Dialog.Text = "Open Connection " & NewIPAddress.ToString & ", " & NewIPAddress.AddressFamily.ToString & vbCrLf & Me.TextBox_Dialog.Text
						Me.Refresh()

						Client.Connect(NewIPAddress, TCPServerGlobals.FCP_ServerPort)

						Exit For

					Catch ex As Exception
						Me.TextBox_Dialog.Text = "Request Denied " & ex.Message & vbCrLf & Me.TextBox_Dialog.Text
						Me.Refresh()

						NewIPAddress = Nothing
					End Try
				Next

				If (NewIPAddress IsNot Nothing) Then

					Stream = Client.GetStream()

					w = New BinaryWriter(Stream)
					r = New BinaryReader(Stream)

					' Start a dialogue.

          PostToMessageServer(Stream, TCP_ServerMessageType.Client_RequestConnect, FCP_Application.Debug, TextBox_Instance.Text, System.Security.Principal.WindowsIdentity.GetCurrent().Name)

					Me.TextBox_Dialog.Text = "Request Connect" & vbCrLf & Me.TextBox_Dialog.Text
					Me.Refresh()

				End If

			Catch ex As Exception
				Me.TextBox_Dialog.Text = "Request Denied " & ex.Message & vbCrLf & Me.TextBox_Dialog.Text
				Me.Refresh()

				Try
					Client.Close()
				Catch ex1 As Exception
				Finally
					Stream = Nothing
				End Try

			End Try

    End If
  End Sub

  Private Sub Button_Disconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Disconnect.Click
    Try
      Me.TextBox_Dialog.Text = "Closing Connection" & vbCrLf & Me.TextBox_Dialog.Text
      Me.Refresh()

      PostToMessageServer(Stream, TCP_ServerMessageType.Client_Disconnect, FCP_Application.Venice, TextBox_Instance.Text, "")
      Client.Close()
      Stream = Nothing
    Catch ex As Exception
      Try
        Client.Close()
      Catch ex1 As Exception
      Finally
        Stream = Nothing
        Me.TextBox_Dialog.Text = "Error, Conn Closed." & vbCrLf & Me.TextBox_Dialog.Text
        Me.Refresh()
      End Try
    End Try
  End Sub



  'Private Sub Button_SendInput_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SendInput.Click
  '    Try
  '        Me.TextBox_Dialog.Text = "Send : '" & Me.TextBox_Server.Text & "'" & vbCrLf & Me.TextBox_Dialog.Text
  '        Me.Refresh()

  '        w.Write(Me.TextBox_Server.Text)
  '    Catch ex As Exception
  '        Try
  '            Client.Close()
  '        Catch ex1 As Exception
  '        Finally
  '            Stream = Nothing
  '            Me.TextBox_Dialog.Text = "Error, Conn Closed." & vbCrLf & Me.TextBox_Dialog.Text
  '            Me.Refresh()
  '        End Try

  '    End Try

  'End Sub

  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Dim MessageType As TCP_ServerMessageType
    Dim InstanceName As String
    Dim ApplicationType As FCP_Application
    Dim ReadString As String
    Dim ErrorFlag As Boolean
    Dim MessageHeader As TCPServerHeader
    Dim OutputString As String


    If (Not (Stream Is Nothing)) AndAlso (Stream.DataAvailable) Then
      ErrorFlag = False

      While (ErrorFlag = False) AndAlso (Stream.DataAvailable)
        Try
          MessageHeader = DirectCast(SerializeFormatter.Deserialize(Stream), TCPServerHeader)

          MessageType = MessageHeader.MessageType
          ApplicationType = MessageHeader.Application
          ReadString = MessageHeader.MessageString
          InstanceName = MessageHeader.InstanceName

          OutputString = "(Rcvd " & Now.ToString("HH:mm:ss") & ") [" & IIf(InstanceName.Length > 0, InstanceName, "<none>").ToString() & "]" & System.Enum.GetName(GetType(TCP_ServerMessageType), MessageType) & ", " & _
                      System.Enum.GetName(GetType(FCP_Application), ApplicationType) & ", " & _
                      ReadString

          Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
          Me.Refresh()

          Dim FollowingObjects() As Object

          ' Take in Following objects, if any.

          If MessageHeader.FollowingMessageCount > 0 Then
            Dim Counter As Integer

            ReDim FollowingObjects(MessageHeader.FollowingMessageCount - 1)

            For Counter = 1 To MessageHeader.FollowingMessageCount
              Try
                FollowingObjects(Counter - 1) = SerializeFormatter.Deserialize(Stream)
              Catch ex As Exception
                FollowingObjects(Counter - 1) = Nothing
              End Try
            Next

            Dim ThisObject As Object

            For Each ThisObject In FollowingObjects
              If Not (ThisObject Is Nothing) Then

                OutputString = "(Rcvd         ) > " & ThisObject.GetType.ToString
                Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                Me.Refresh()

                If ThisObject.GetType Is GetType(TCPServerObjectWrapper) Then
                  If (CType(ThisObject, TCPServerObjectWrapper).ObjectData Is Nothing) Then
                    ThisObject = "<Nothing>"
                  Else
                    ThisObject = SerializeFormatter.Deserialize(New MemoryStream(CType(ThisObject, TCPServerObjectWrapper).ObjectData))
                  End If

                  OutputString = "(Rcvd         ) > Type : " & ThisObject.GetType.ToString
                  Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                  Me.Refresh()
                End If


                If ThisObject.GetType Is GetType(NaplesGlobals.NaplesDataAnswer) Then
                  Dim ThisAnswer As NaplesDataAnswer
                  ThisAnswer = CType(ThisObject, NaplesDataAnswer)
                  LastAnswer = ThisAnswer

                  OutputString = "(Rcvd         )   > " & System.Enum.GetName(GetType(NaplesGlobals.NaplesAnswerType), ThisAnswer.AnswerType)
                  Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                  Me.Refresh()

                  If Not (ThisAnswer.AnswerObject Is Nothing) Then
                    OutputString = "(Rcvd         )     > AnswerObject type = " & ThisAnswer.AnswerObject.GetType.ToString
                    Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                    Me.Refresh()

                    If (ThisAnswer.AnswerObject.GetType Is GetType(Hashtable)) Then

                      Dim Item As DictionaryEntry
                      For Each Item In CType(ThisAnswer.AnswerObject, Hashtable)
                        If (Item.Value.GetType Is GetType(DataServerRTField)) Then
                          Dim FieldName As String

                          FieldName = CType(Item.Value, DataServerRTField).FieldName
                          If CType(Item.Value, DataServerRTField).FieldValue.GetType Is GetType(System.String) Then
                            OutputString = "(Rcvd         )   > " & FieldName & ", " & CType(CType(Item.Value, DataServerRTField).FieldValue, System.String)
                          ElseIf CType(Item.Value, DataServerRTField).FieldValue.GetType Is GetType(System.DateTime) Then
                            OutputString = "(Rcvd         )   > " & FieldName & ", " & CType(CType(Item.Value, DataServerRTField).FieldValue, System.DateTime).ToString("dd MMM yyyy")
                          Else
                            OutputString = "(Rcvd         )   > " & FieldName & ", " & CType(CType(Item.Value, DataServerRTField).FieldValue, System.Double).ToString
                          End If

                          Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                          Me.Refresh()

                        End If
                      Next

                    ElseIf (ThisAnswer.AnswerObject.GetType Is GetType(ArrayList)) Then
                      Dim ItemCounter As Integer
                      Dim thisArrayList As ArrayList

                      thisArrayList = CType(ThisAnswer.AnswerObject, ArrayList)

                      TextBox_Dialog.SuspendLayout()
                      For ItemCounter = (thisArrayList.Count - 1) To 0 Step -1
                        OutputString = "(Rcvd         )   > " & thisArrayList(ItemCounter).ToString

                        Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                      Next
                      TextBox_Dialog.ResumeLayout()
                      Me.Refresh()

                    ElseIf (ThisAnswer.AnswerObject.GetType Is GetType(String)) Then
                      OutputString = CType(ThisAnswer.AnswerObject, String)
                      Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                      Me.Refresh()

                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_ReportDate, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_Header, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_InstrumentLines, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_CurrentPage, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_TotalPages, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_Currency, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_HolderName, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_SettleOn, CType(ThisAnswer.AnswerObject, String))
                      OutputString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PSDP_PortfolioID, CType(ThisAnswer.AnswerObject, String))
                    End If
                  End If


                ElseIf ThisObject.GetType Is GetType(TCPServerHeader) Then
                  Dim ThisAnswer As TCPServerHeader
                  ThisAnswer = CType(ThisObject, TCPServerHeader)

                ElseIf ThisObject.GetType Is GetType(RenaissanceUpdateClass) Then
                  Dim ThisUpdate As RenaissanceUpdateClass
                  Dim ThisChangeID As RenaissanceChangeID

                  ThisUpdate = CType(ThisObject, RenaissanceUpdateClass)

                  For Each ThisChangeID In ThisUpdate.ChangeIDs
                    If (ThisChangeID <> RenaissanceChangeID.None) Then
                      OutputString = "(Rcvd         )     > " & ThisChangeID.ToString & ", Detail : " & ThisUpdate.UpdateDetail(ThisChangeID)
                      Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                      Me.Refresh()
                    End If
                  Next

                ElseIf ThisObject.GetType Is GetType(TCPServerObjectWrapper) Then
                  Dim thisObjectWrapper As TCPServerObjectWrapper = CType(ThisObject, TCPServerObjectWrapper)
                  OutputString = "(Rcvd         )     > Type : " & thisObjectWrapper.ObjectType & ", Format : " & thisObjectWrapper.ObjectFormat
                  Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                  Me.Refresh()

                  Dim thisObjectTest As New ObjectTest
                  If thisObjectWrapper.ObjectType = "ObjectTest" Then
                    Dim ms As New MemoryStream(thisObjectWrapper.ObjectData)

                    thisObjectTest = CType(SerializeFormatter.Deserialize(ms), ObjectTest)
                    OutputString = "(Rcvd         )     > ObjectTest : V1:" & thisObjectTest.V1.ToString & ", S1:" & thisObjectTest.S1 & ", D1:" & thisObjectTest.D1.ToString
                    Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
                    Me.Refresh()

                  End If

                End If

              End If

            Next

          End If


          If MessageType = TCP_ServerMessageType.Server_Touchbase Then
            PostToMessageServer(Stream, TCP_ServerMessageType.Client_AcknowledgeOK, FCP_Application.Debug, TextBox_Instance.Text, "")
          ElseIf MessageType = TCP_ServerMessageType.Server_ApplicationUpdateMessage Then
            PostToMessageServer(Stream, TCP_ServerMessageType.Client_AcknowledgeOK, FCP_Application.Debug, TextBox_Instance.Text, "")
          End If
        Catch ex As Exception
          ErrorFlag = True
          Me.TextBox_Dialog.Text = "     Error : " & ex.Message & vbCrLf & Me.TextBox_Dialog.Text
          Me.Refresh()

          'Try
          '  Client.Close()
          'Catch ex1 As Exception
          'Finally
          '  Stream = Nothing
          'End Try
        End Try
      End While
    End If

  End Sub

  Private Sub Button_SendMessage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SendMessage.Click

    Dim MessageType As TCP_ServerMessageType
    Dim ApplicationType As FCP_Application
    Dim MessageString As String
    Dim OutputString As String

    If Me.CB_Message.SelectedIndex >= 0 Then
      MessageType = CType(System.Enum.Parse(GetType(TCP_ServerMessageType), Me.CB_Message.Text), TCP_ServerMessageType)
    Else
      MessageType = TCP_ServerMessageType.Client_Touchbase
    End If

    If Me.CB_Application.SelectedIndex >= 0 Then
      ApplicationType = CType(System.Enum.Parse(GetType(FCP_Application), Me.CB_Application.Text), FCP_Application)
    Else
      ApplicationType = FCP_Application.None
    End If

    MessageString = Me.Text_MessageText.Text

    OutputString = "(Sent) " & System.Enum.GetName(GetType(TCP_ServerMessageType), MessageType) & ", " & _
                System.Enum.GetName(GetType(FCP_Application), ApplicationType) & ", " & _
               MessageString
    Me.TextBox_Dialog.Text = OutputString & vbCrLf & Me.TextBox_Dialog.Text
    Me.Refresh()


    ' LastAnswer

    If MessageType = TCP_ServerMessageType.Client_NaplesDataRequest Then
      Dim thisMessageHeader As TCPServerHeader = New TCPServerHeader(MessageType, ApplicationType, TextBox_Instance.Text, MessageString, 1, 0)

      If CType(System.Enum.Parse(GetType(DS_RequestType), Me.CB_DataRequestType.Text), DS_RequestType) = DS_RequestType.Cancel Then
        LastAnswer.RequestObject.RequestType = DS_RequestType.Cancel
        PostToMessageServer(Stream, CType(thisMessageHeader, Object))
        PostToMessageServer(Stream, AddTCPServerWrapperClass(CType(LastAnswer.RequestObject, Object)))
      Else
        Dim DataRequest As Object = New NaplesGlobals.NaplesDataRequest(MessageString, CType(System.Enum.Parse(GetType(DS_RequestType), Me.CB_DataRequestType.Text), DS_RequestType))
        PostToMessageServer(Stream, CType(thisMessageHeader, Object))
        PostToMessageServer(Stream, AddTCPServerWrapperClass(DataRequest))
        DataRequest = Nothing
      End If

    Else
      Dim thisMessageHeader As TCPServerHeader = New TCPServerHeader(MessageType, ApplicationType, TextBox_Instance.Text, MessageString, 0, 0)
      PostToMessageServer(Stream, CType(thisMessageHeader, Object))

    End If

  End Sub

  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    'Dim thisMessageHeader As TCPServerHeader = New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, FCP_Application.Venice, "", 1, 0)
    '   Dim thisObjectWrapper As New TCPServerObjectWrapper
    '	Dim thisObject As New RenaissanceUpdateClass
    'Dim thisarray() As String = {"tblFund"}
    'thisObject.Set_ChangeIDs(thisarray)

    'thisObjectWrapper.ObjectType = "VeniceUpdateClass"
    '   thisObjectWrapper.ObjectFormat = "BinaryFormatter"

    '   Dim ms As New MemoryStream
    '   SerializeFormatter.Serialize(ms, thisObject)
    '   Dim sr As New StreamReader(ms)
    '   ms.Position = 0
    '   Dim BArray() As Byte
    '   BArray = ms.ToArray()
    '   thisObjectWrapper.ObjectData = BArray

    '   PostServerMessage(Stream, CType(thisMessageHeader, Object))
    '   PostServerMessage(Stream, CType(thisObjectWrapper, Object))

    Dim myAddresses As System.Net.IPHostEntry

    myAddresses = Dns.GetHostEntry(System.Environment.MachineName)

    ' Loop on the AddressList
    Dim curAdd As IPAddress
    Dim oString As String
    Dim counter As Integer = 1

    For Each curAdd In myAddresses.AddressList
      oString = ""
      oString &= (counter & ")") & vbCrLf

      ' Display the type of address family supported by the server. If the
      ' server is IPv6-enabled this value is: InternNetworkV6. If the server
      ' is also IPv4-enabled there will be an additional value of InterNetwork.
      Console.WriteLine(("AddressFamily: " + curAdd.AddressFamily.ToString()))
      oString &= ("AddressFamily: " + curAdd.AddressFamily.ToString()) & vbCrLf

      ' Display the ScopeId property in case of IPV6 addresses.
      If curAdd.AddressFamily.ToString() = ProtocolFamily.InterNetworkV6.ToString() Then
        Console.WriteLine(("Scope Id: " + curAdd.ScopeId.ToString()))

        oString &= ("Scope Id: " + curAdd.ScopeId.ToString()) & vbCrLf

      End If

      ' Display the server IP address in the standard format. In 
      ' IPv4 the format will be dotted-quad notation, in IPv6 it will be
      ' in in colon-hexadecimal notation.
      Console.WriteLine(("Address: " + curAdd.ToString()))
      oString &= ("Address: " + curAdd.ToString()) & vbCrLf

      ' Display the server IP address in byte format.
      Console.Write("AddressBytes: ")
      oString &= "AddressBytes: " & vbCrLf

      Dim bytes As [Byte]() = curAdd.GetAddressBytes()
      Dim i As Integer
      For i = 0 To bytes.Length - 1
        Console.Write(bytes(i))
        oString &= bytes(i) & "."
      Next i

      Console.WriteLine(ControlChars.Cr + ControlChars.Lf)
      oString &= vbCrLf

      Me.TextBox_Dialog.Text = oString & vbCrLf & Me.TextBox_Dialog.Text

      Me.Refresh()
      counter += 1
    Next curAdd




  End Sub

  <Serializable()> Private Class ObjectTest
    Public V1 As Integer
    Public S1 As String
    Public D1 As Double
  End Class

  Public Function AddTCPServerWrapperClass(ByRef pSourceObject As Object) As Object
    Dim SerializeFormatter As New BinaryFormatter
    Dim ThisWrapperObject As TCPServerObjectWrapper

    If (Not (pSourceObject.GetType Is GetType(TCPServerObjectWrapper))) Then
      Try
        ThisWrapperObject = New TCPServerObjectWrapper
        ThisWrapperObject.ObjectType = pSourceObject.GetType.Name
        ThisWrapperObject.ObjectFormat = "BinaryFormatter"

        Dim ms As New MemoryStream
        SerializeFormatter.Serialize(ms, pSourceObject)
        ms.Position = 0
        ThisWrapperObject.ObjectData = ms.ToArray()
        ms.Close()
      Catch ex As Exception
        ThisWrapperObject = New TCPServerObjectWrapper
      End Try

    Else
      Return pSourceObject
    End If

    Return CType(ThisWrapperObject, Object)

  End Function

  Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
    Dim ApplicationType As FCP_Application

    If Me.CB_Application.SelectedIndex >= 0 Then
      ApplicationType = CType(System.Enum.Parse(GetType(FCP_Application), Me.CB_Application.Text), FCP_Application)
    Else
      ApplicationType = FCP_Application.None
    End If

    If MessageBox.Show("Are you sure that you want to terminate " & ApplicationType.ToString & " ?", "Terminate ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
      Exit Sub
    End If

    Dim thisMessageHeader As TCPServerHeader = New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationType, TextBox_Instance.Text, "", 1, 0)
    Dim ApplicationUpdate As New RenaissanceUpdateClass

    ApplicationUpdate.Set_ChangeIDs(New RenaissanceChangeID() {RenaissanceChangeID.Terminate1, RenaissanceChangeID.Terminate2})

    PostToMessageServer(Stream, CType(thisMessageHeader, Object))
    PostToMessageServer(Stream, CType(ApplicationUpdate, Object))

  End Sub



  Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
    TextBox_Dialog.Text = ""
  End Sub
End Class
