Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports TCPServer_Common
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Public Class PostMessageUtils

    Private Shared SerializeFormatter As New BinaryFormatter

    ' *********************************************************************
    ' General purpose functions to send Server messages to TCP connections
    ' *********************************************************************

  Public Shared Function PostToMessageServer(ByRef pThisConnection As NetworkStream, ByRef pMessageHeader As Object) As Boolean
    Try

      If (pThisConnection IsNot Nothing) AndAlso (pMessageHeader IsNot Nothing) Then

        SyncLock pThisConnection
          SyncLock SerializeFormatter

            Dim ms As New MemoryStream
            SerializeFormatter.Serialize(ms, pMessageHeader)
            ms.Position = 0
            ms.WriteTo(pThisConnection)
            ms.Close()

            'SerializeFormatter.Serialize(pThisConnection, pMessageHeader)
            'pThisConnection.Flush()
          End SyncLock
        End SyncLock

      End If
    Catch ex As Exception
      Return False
    End Try

    Return True
  End Function

  Public Shared Function PostToMessageServer( _
      ByRef pThisConnection As NetworkStream, _
      ByVal pMessage As TCP_ServerMessageType, _
      ByVal pApplication As FCP_Application, _
      ByVal pInstanceName As String, _
      ByVal pMessageString As String) As Boolean

    Return PostToMessageServer(pThisConnection, New TCPServerHeader(pMessage, pApplication, pInstanceName, pMessageString, 0, 0))
  End Function


End Class

