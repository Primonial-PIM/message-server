Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports TCPServer_Common

Module UtilsAndDebug

  Friend SerializeFormatter As New BinaryFormatter

  ' *********************************************************************
  ' General purpose functions to send Server messages to TCP connections
  ' *********************************************************************

  Friend Function PostToMessageServer(ByRef pThisConnection As TcpConnClass, ByVal pMessageObject As Object) As Boolean

    If (pThisConnection IsNot Nothing) AndAlso (pThisConnection.Stream IsNot Nothing) AndAlso (pMessageObject IsNot Nothing) Then

      Return PostMessageUtils.PostToMessageServer(pThisConnection.Stream, pMessageObject)

    End If

  End Function

  Friend Function PostToMessageServer(ByRef pThisConnection As TcpConnClass, ByVal pMessage As TCP_ServerMessageType) As Boolean

    If (pThisConnection IsNot Nothing) AndAlso (pThisConnection.Stream IsNot Nothing) Then

      Return PostMessageUtils.PostToMessageServer(pThisConnection.Stream, New TCPServerHeader(pMessage, FCP_Application.None, pThisConnection.InstanceName, "", 0, 0))

    End If

  End Function

  Friend Function PostToMessageServer(ByRef pThisConnection As TcpConnClass, ByVal pMessageHeader As TCPServerHeader) As Boolean

    If (pThisConnection IsNot Nothing) AndAlso (pThisConnection.Stream IsNot Nothing) AndAlso (pMessageHeader IsNot Nothing) Then

      Return PostMessageUtils.PostToMessageServer(pThisConnection.Stream, CType(pMessageHeader, Object))

    End If

  End Function

  Friend Function PostToMessageServer( _
        ByRef pThisConnection As TcpConnClass, _
        ByVal pMessage As TCP_ServerMessageType, _
        ByVal pApplication As FCP_Application, _
        ByVal pInstanceName As String, _
        ByVal pMessageString As String) As Boolean

    If (pThisConnection IsNot Nothing) AndAlso (pThisConnection.Stream IsNot Nothing) Then

      Return PostMessageUtils.PostToMessageServer(pThisConnection.Stream, New TCPServerHeader(pMessage, pApplication, pInstanceName, pMessageString, 0, 0))

    End If

  End Function


  ' *********************************************************************
  ' Functions to post Debug messages
  ' *********************************************************************

  Friend Sub PostDebugMessage(ByRef DS As TCPServerClass, ByRef pMessageHeader As TCPServerHeader)
    '
    ' Function to post debug messages to all connections that have registered as debug users
    '
    ' This function also maintains the 'LiveDebugs' flag in the TCPServerClass object. This flag is designed to be maintained
    ' as True if there are any live Debug connections, but be set to False if there are none.
    ' The purpose of this is simply to improve the performance of the PostDebugMessage() function so that 
    ' the Connections collection is not looped through repeatedly if there are no live Debug connections.
    '

    If DS.LiveDebugs = False Then Exit Sub

    Dim DebugCount As Integer
    Dim MessageType As String
    Dim Application As String
    Dim DebugString As String

    Dim PropogateConnClass As TcpConnClass
    Dim newTCPServerHeader As TCPServerHeader

    ' Use given Message Header if it is a Debug Message, or create new Debug Message from the given MessageHeader.

    If pMessageHeader.MessageType = TCP_ServerMessageType.Server_DebugMessage Then
      newTCPServerHeader = pMessageHeader
    Else
      Try
        MessageType = System.Enum.GetName(GetType(TCP_ServerMessageType), pMessageHeader.MessageType)
      Catch ex As Exception
        MessageType = "<Unknown Message Type>"
      End Try

      Try
        Application = pMessageHeader.Application.ToString
      Catch ex As Exception
        Application = "<Unknown Application Type>"
      End Try

      DebugString = "(?) Type : " & MessageType & ", Application : " & Application & ", MessageString : '" & pMessageHeader.MessageString & "'"

      newTCPServerHeader = New TCPServerHeader(TCP_ServerMessageType.Server_DebugMessage, FCP_Application.Debug, pMessageHeader.InstanceName, DebugString, 0, 0)
    End If

    DebugCount = 0

    For Each PropogateConnClass In DS.ConnColl
      If (PropogateConnClass.ActiveConnection = True) AndAlso (PropogateConnClass.DeleteMe = False) Then
        If (PropogateConnClass.ApplicationType And FCP_Application.Debug) = FCP_Application.Debug Then
          DebugCount += 1
          Call PostToMessageServer(PropogateConnClass, newTCPServerHeader)
        End If
      End If
    Next

    If (DebugCount <= 0) Then
      DS.LiveDebugs = False
    End If
  End Sub

  Friend Sub PostDebugMessage(ByRef DS As TCPServerClass, ByVal pDebugMessage As String)
    ' Post Standard Debug Message

    Call PostDebugMessage(DS, New TCPServerHeader(TCP_ServerMessageType.Server_DebugMessage, FCP_Application.Debug, "", pDebugMessage, 0, 0))

  End Sub

  Friend Sub PostDebugMessage(ByRef DS As TCPServerClass, ByRef pThisConnection As TcpConnClass, ByRef pMessageHeader As TCPServerHeader)
    '
    ' Function to post debug messages to all connections that have registered as debug users
    '

    If DS.LiveDebugs = False Then Exit Sub

    Dim MessageType As String
    Dim Application As String
    Dim DebugString As String

    Dim newTCPServerHeader As TCPServerHeader

    ' Use given Message Header if it is a Debug Message, or create new Debug Message from the given MessageHeader.

    If pMessageHeader.MessageType = TCP_ServerMessageType.Server_DebugMessage Then
      newTCPServerHeader = pMessageHeader
    Else
      Try
        MessageType = System.Enum.GetName(GetType(TCP_ServerMessageType), pMessageHeader.MessageType)
      Catch ex As Exception
        MessageType = "<Unknown Message Type>"
      End Try

      Try
        Application = pMessageHeader.Application.ToString
      Catch ex As Exception
        Application = "<Unknown Application Type>"
      End Try

      DebugString = "(" & pThisConnection.ConnectionID.ToString & ") Type : " & MessageType & ", Application : " & Application & ", MessageString : '" & pMessageHeader.MessageString & "'"

      newTCPServerHeader = New TCPServerHeader(TCP_ServerMessageType.Server_DebugMessage, FCP_Application.Debug, pMessageHeader.InstanceName, DebugString, 0, 0)
    End If

    Call PostDebugMessage(DS, newTCPServerHeader)

  End Sub

End Module
