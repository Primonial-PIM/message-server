Imports System.ServiceProcess
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports TCPServer_Common
Imports System.Threading

' *************************************************************************************************
'
' Renaissance Application Message Server.
'
' *************************************************************************************************
'
' Background : 
' The GlobalAutoUpdate process within Venice is designed such that if one user makes a change, then
' other users are notified and the application updates as necessary.
' e.g. If one user adds a new Instrument, then that new Instrument will be instantly reflected in all 
' other users forms and drop-downs, etc.
' Venice Version One achieved this by polling for changes in a dedicated dataset, designed for the purpose.
' 
' The Present :
' For the re-write of Venice, I decided that a less DB reliant solution would be appropriate.
' Thus this TCP server has been devised. The primary function is to accept connections from various 
' applications and to relay messages between these connections.
'
' The Application has been written to utilise a single thread, it will look for new connections and process
' existing connections on a round-robin basis, All active connections being held within a TcpConnClass 
' class structure and held in a collection.
' It explicitly uses a single thread in order that the number of live threads does not become excessive as 
' the number of connections increases. This approach is of course open to review.
'
' The scope of the application expanded from the initial Venice-Only approach to become a more general purpose
' Application message server for any Renaissance application.
' One a connection is made, the Client will register itself as being of one or more application types. This
' registration will enable it to send and receive messages relating to that Application type.
' A Client that is not registered for a given Application is unable to send messages (ignored) and will
' not receive messages from other clients relating to that Application type.
' 
' The Server will periodically poll the clients with an Are-You-There message in order to keep the connection 
' fresh and so that the server can discard dead connections.
'
' All the Server and Client messages or pre-ambles are defined in the TCPServer_Common Class.
' As of writing, it contains :
'
' Server Messages :
'


Public Class FCP_MessageServer
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New FCP_MessageServer}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
	'
	'FCP_MessageServer
	'
    Me.ServiceName = "FCP_MessageServer"
    Me.CanStop = True
    Me.CanPauseAndContinue = False
    Me.AutoLog = True

  End Sub

#End Region

  Private TcpServer As TCPServerClass

  Protected Overrides Sub OnStart(ByVal args() As String)
	' *******************************************************************************
	' Initiate TCP Server
	' 
	' 
	' *******************************************************************************

	' *******************************************************************************
	' Check for previous instances
	' *******************************************************************************

	Dim Proc() As Process

	Dim ModuleName As String
	Dim ProcName As String

	' Get Current Process details
	ModuleName = Process.GetCurrentProcess.MainModule.ModuleName
	ProcName = System.IO.Path.GetFileNameWithoutExtension(ModuleName)

	' Find all processes with this name
	Proc = Process.GetProcessesByName(ProcName)
	If Proc.Length > 1 Then
	  Exit Sub
	End If

	' *******************************************************************************
	' Start up
	' *******************************************************************************

	' Start TCPServerClass
	TcpServer = New TCPServerClass
	TcpServer.Start()

  End Sub

  Protected Overrides Sub OnStop()
	Dim TimeCounter As Integer

	' ShutDown TCPServer, Allow it 200ms to close

	TcpServer.CloseDown = True
	TimeCounter = 20
	While (TimeCounter > 0) And (TcpServer.IsCompleted = False)
	  Thread.Sleep(10)
	  TimeCounter -= 1
	End While
	If (TcpServer.IsCompleted = False) Then
	  TcpServer.Stop()
	End If

  End Sub

End Class

