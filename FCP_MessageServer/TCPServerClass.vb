Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Imports TCPServer_Common

Friend Class TCPServerClass
  ' *****************************************************************************************************
  ' DATA SERVER CLASS
  ' -----------------
  '
	' This class represents the message passing mechanism and thus is the heart of the Renaissance suite of 
  ' Applications.
  '
  ' In essence the function is simple : Accept connections from Client and DataServer applications and 
  ' pass messages between them as necessary.
  ' The first comcept was simply to pass Venice AutoUpdate messages between instances of the Venice 
  ' application so that changes made by one user might be reflected on other user's screens.
  ' This concept has been extended to allow this type of messaging between any application type.
  '
  ' Subsequently came the concept of passing Requests to / Answers from various Data Servers. Initially 
  ' it was thought that this would be used for Market Data, but the concept was extended to inclue a 
  ' Server for Venice Data - e.g. Valuations and Reports.
  ' 
  '
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

  Private _CloseDownFlag As Boolean
  Friend LiveDebugs As Boolean

  Friend ConnColl As New Collection
  Friend ConnectionsToDelete As New ArrayList
	Private xListener As TcpListener
	Private ListenerArray As New Collection

  Private SerializeFormatter As New BinaryFormatter
  Private ThisServerGUID As Guid = Guid.NewGuid

  Public Property CloseDown() As Boolean
	Get
	  Return _CloseDownFlag
	End Get
	Set(ByVal Value As Boolean)
	  _CloseDownFlag = Value
	End Set
  End Property

  Protected Overrides Sub DoTask()

	Initialise()

	RunTCPServer()

	TidyUp()

  End Sub

  Private Sub Initialise()
    Dim myAddresses As System.Net.IPHostEntry
    Dim haveConnected As Boolean = False
    Dim curAdd As IPAddress
		Dim thisListener As TcpListener

    _CloseDownFlag = False
    LiveDebugs = False

    Try
      myAddresses = Dns.GetHostEntry(System.Environment.MachineName)

      ' Try to listen on RENAISSANCE_NETWORK_FAMILY (ip4 at time of writing) port for preference

			For Each curAdd In myAddresses.AddressList
				Try

					thisListener = New TcpListener(curAdd, TCPServerGlobals.FCP_ServerPort)
					ListenerArray.Add(thisListener)
					thisListener.Start()
					haveConnected = True

					'If curAdd.AddressFamily = TCPServer_Common.TCPServerGlobals.RENAISSANCE_NETWORK_FAMILY Then
					'	Listener = New TcpListener(curAdd, TCPServerGlobals.FCP_ServerPort)
					'	haveConnected = True
					'End If

				Catch ex As Exception
				End Try
			Next

      ' Listen on IP6 as an alternative.

			'If (haveConnected = False) Then
			'  For Each curAdd In myAddresses.AddressList
			'    If (haveConnected = False) Then
			'      If curAdd.AddressFamily = System.Net.Sockets.AddressFamily.InterNetworkV6 Then
			'        Listener = New TcpListener(curAdd, TCPServerGlobals.FCP_ServerPort)
			'        haveConnected = True
			'      End If
			'    End If
			'  Next
			'End If

			'Listener.Start()
		Catch ex As Exception
			If (haveConnected = False) Then
				_CloseDownFlag = True
			End If
		End Try

  End Sub

  Private Sub TidyUp()
	' On ending the service

	Try
	  Dim newTcpConnClass As TcpConnClass

	  PostDebugMessage(Me, "Server Shutting Down. Closing all connections.")

	  For Each newTcpConnClass In ConnColl
        Try
          SerializeFormatter.Serialize(newTcpConnClass.Stream, New TCPServerHeader(TCP_ServerMessageType.Server_Disconnect, FCP_Application.None, newTcpConnClass.InstanceName, "", 0, 0))
          newTcpConnClass.Stream.Flush()
          newTcpConnClass.Client.Close()
        Catch ex As Exception
        End Try

		Try
		  ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
		Catch ex As Exception
		End Try
	  Next

	Catch ex As Exception
	End Try

		Try
			Dim thisListener As TcpListener

			For Each thisListener In ListenerArray
				thisListener.Stop()
			Next
			'Listener.Stop()

			PostDebugMessage(Me, "Listener stopped.")

		Catch ex As Exception
		End Try

  End Sub

  Private Sub RunTCPServer()
    '
    ' Function to provide all the TCP Server functionality.
    '
    ' Principally the server will accept and manage TCP connections on the relevant TCP Ports and then pass
    ' messages between connections as appropriate.
    '
    Dim MessageHeader As TCPServerHeader
    Dim MessageObject As Object

    Dim newTcpConnClass As TcpConnClass
    Dim thisTcpConnClass As TcpConnClass

    Dim Client As TcpClient
    Dim Stream As NetworkStream

    Dim ConnCounter As Integer
    Dim ConnCounterOK As Boolean
    Dim LoopWorkDone As Boolean
		Dim thisListener As TcpListener

    ' Loop until the server is told to close down gracefully.

    ConnCounter = 0
    While _CloseDownFlag = False

      ' *************************
      ' Check for new connections
      ' *************************
      LoopWorkDone = False

      Try

        ' If there is a connection pending...

				For Each thisListener In ListenerArray

					If thisListener.Pending = True Then
						LoopWorkDone = True

						' Find unused ConnCounter, Counter will count upwards for each new connection, but will return to One and re-use
						' numbers eventually.

						ConnCounterOK = False
						While ConnCounterOK = False
							If ConnCounter >= System.Int32.MaxValue Then ConnCounter = 0
							ConnCounter += 1
							ConnCounterOK = True

							For Each thisTcpConnClass In ConnColl
								If thisTcpConnClass.ConnectionID = ConnCounter Then
									ConnCounterOK = False
								End If
							Next
						End While

						' Initialise the new connection. Allocate a connection ID and GUID and add the new connection to the Connections collection.

						Client = thisListener.AcceptTcpClient()
						Client.ReceiveTimeout = TCPServerGlobals.FCP_Server_ReceiveTimeout
						Client.SendTimeout = TCPServerGlobals.FCP_Server_SendTimeout
						Client.NoDelay = True
						Client.ReceiveBufferSize = (2 ^ 14)	' 16K
						Client.SendBufferSize = (2 ^ 14) ' 16K
						PostDebugMessage(Me, "(" & ConnCounter.ToString & ") Connection accepted. ")

						' Retrieve the network stream.
						Stream = Client.GetStream()

						newTcpConnClass = New TcpConnClass

						newTcpConnClass.ConnectionID = ConnCounter
						newTcpConnClass.Stream = Stream
						newTcpConnClass.Client = Client
						newTcpConnClass.bReader = New BinaryReader(Stream)
						newTcpConnClass.bWriter = New BinaryWriter(Stream)
						newTcpConnClass.GUID = Guid.NewGuid
						newTcpConnClass.WakeupTime = Now
						newTcpConnClass.ClientIPAddress = CType(Client.Client.RemoteEndPoint, System.Net.IPEndPoint).Address

						ConnColl.Add(newTcpConnClass, newTcpConnClass.ConnectionID.ToString)

					End If

				Next

      Catch ex As Exception
        PostDebugMessage(Me, "Error establishing new connection : " & Err.ToString())
      End Try

      ' ****************************
      ' Process existing connections
      ' ****************************

      Dim MessageCounter As Integer

      For Each newTcpConnClass In ConnColl
        Try

          MessageCounter = 0

          ' If this connection is Live AND there is pending data AND this Connection is not taking too much time, then

          While (Not (newTcpConnClass Is Nothing)) AndAlso (newTcpConnClass.DeleteMe = False) AndAlso (newTcpConnClass.Stream.DataAvailable = True) AndAlso (MessageCounter < 5)
            MessageCounter += 1
            newTcpConnClass.WakeupTime = Now()
            LoopWorkDone = True

            ' Read Message Header
            MessageObject = Nothing
            Try
              Threading.Thread.Sleep(50)

              MessageObject = SerializeFormatter.Deserialize(newTcpConnClass.Stream)

            Catch ex As SerializationException
              MessageObject = Nothing
            Catch ex As Exception
              MessageObject = Nothing

              PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Error Deserialising from Stream : " & ex.Message)

              Try
                newTcpConnClass.DeleteMe = True
                ConnectionsToDelete.Add(newTcpConnClass)
              Catch ex1 As Exception
              End Try
            End Try

            Try
              If (Not (MessageObject Is Nothing)) AndAlso (MessageObject.GetType Is GetType(TCPServerHeader)) Then
                MessageHeader = DirectCast(MessageObject, TCPServerHeader)

                ' Backwards compatability....
                If (MessageHeader.InstanceName Is Nothing) Then
                  MessageHeader.InstanceName = ""
                End If

                ' OK, on we go....
                If (newTcpConnClass.ActiveConnection = False) AndAlso _
                  (Not ((MessageHeader.MessageType = TCP_ServerMessageType.Client_RequestConnect) Or (MessageHeader.MessageType = TCP_ServerMessageType.Server_RequestConnect))) Then
                  ' If This connection has not been 'Initiated' by a 'RequestConnect' Message and this is not a 'RequestConnect' message then bin it.

                  Call BinFollowingMessages(MessageHeader, newTcpConnClass)
                  MessageHeader = Nothing

                Else
                  ' Check that this message has not passed throught this server already. i.e. prevent message recursion !
                  ' If it has, then bin it.
                  ' If this message has passed through this server already and then to another server, then
                  ' Because this server changes it's connection GUID for all new connections, then the GUID of
                  ' this server should appear in the 'PathTravelled' ArrayList.

                  If (MessageHeader.PathTravelled.Count > 0) Then
                    Dim RecursionCounter As Integer

                    For RecursionCounter = 0 To (MessageHeader.PathTravelled.Count - 1)
                      Try
                        If ThisServerGUID.Equals(CType(MessageHeader.PathTravelled(RecursionCounter), Guid)) Then

                          Call BinFollowingMessages(MessageHeader, newTcpConnClass)
                          MessageHeader = Nothing

                          Exit For
                        End If
                      Catch ex As Exception
                      End Try
                    Next
                  End If
                End If
              Else
                MessageHeader = Nothing
              End If

            Catch ex As System.Net.Sockets.SocketException
              MessageHeader = Nothing
              Try
                PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Server SocketException (Connection Lost?) : " & ex.Message)
                newTcpConnClass.Client.Close()
              Catch InnerEx As Exception
              Finally
                newTcpConnClass.DeleteMe = True
                ConnectionsToDelete.Add(newTcpConnClass)
                ' ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
              End Try
              Exit For

            Catch ex As System.IO.IOException
              ' Catch IO Exception. Remove the connection if it is not an established connection

              MessageHeader = Nothing
              Try
                PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Server IOException (Duff Message?) : " & ex.Message)
                If (newTcpConnClass.ActiveConnection = False) Then
                  newTcpConnClass.Client.Close()
                End If
              Catch InnerEx As Exception
              Finally
                If (newTcpConnClass.ActiveConnection = False) Then
                  newTcpConnClass.DeleteMe = True
                  ConnectionsToDelete.Add(newTcpConnClass)
                  ' ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
                End If
              End Try
              Exit For

            Catch ex As InvalidCastException
              MessageHeader = Nothing
              Try
                PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Error Casting to MessageHeader " & ex.Message)
              Catch InnerEx As Exception
              End Try
              Exit For

            Catch ex As Exception
              MessageHeader = Nothing
              Try
                PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Error Reading MessageHeader " & ex.Message)
              Catch InnerEx As Exception
              End Try
              Exit For

            End Try

            ' If a message header has been retrieved.

            If Not (MessageHeader Is Nothing) Then
              ' Append Connection GUID to message if necessary
              Try
                If MessageHeader.PathTravelled.Count = 0 Then
                  MessageHeader.PathTravelled.Add(newTcpConnClass.GUID)
                ElseIf Not newTcpConnClass.GUID.Equals(CType(MessageHeader.PathTravelled(MessageHeader.PathTravelled.Count - 1), Guid)) Then
                  MessageHeader.PathTravelled.Add(newTcpConnClass.GUID)
                End If
              Catch ex As Exception
              End Try

              PostDebugMessage(Me, newTcpConnClass, MessageHeader)

              Try

                Select Case MessageHeader.MessageType

                  Case TCP_ServerMessageType.Client_RegisterApplication
                    ' *******************************
                    ' Register for an Application
                    ' *******************************

                    ' Apply this Application to this connection
                    newTcpConnClass.ApplicationType = newTcpConnClass.ApplicationType Or MessageHeader.Application

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK)

                    If (MessageHeader.Application And FCP_Application.Debug) = FCP_Application.Debug Then
                      LiveDebugs = True
                    End If


                  Case TCP_ServerMessageType.Client_UnRegisterApplication
                    ' *******************************
                    ' Un-Register from an Application
                    ' *******************************

                    ' Remove this applicartion from this connection
                    newTcpConnClass.ApplicationType = newTcpConnClass.ApplicationType And (Not MessageHeader.Application)

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK)


                  Case TCP_ServerMessageType.Client_GetGUID
                    ' ****************************************
                    ' Get GUID - Return Connection GUID to Connection
                    ' ****************************************

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_GUID_Message, FCP_Application.None, MessageHeader.InstanceName, newTcpConnClass.GUID.ToString)


                  Case TCP_ServerMessageType.Client_SetGUID
                    ' ****************************************
                    ' Set GUID - Allow the client to set the connection GUID
                    ' It may be useull if we develop the concept of more persistent connections between clients and servers.
                    ' ****************************************
                    If Not (MessageHeader.MessageString Is Nothing) AndAlso (MessageHeader.MessageString.Length > 0) Then
                      Dim NewGUID As Guid

                      Try
                        NewGUID = New Guid(MessageHeader.MessageString)
                        newTcpConnClass.GUID = NewGUID
                      Catch ex As Exception
                        PostDebugMessage(Me, "    (" & newTcpConnClass.ConnectionID.ToString & ") Client failed to set GUID : '" & MessageHeader.MessageString & "'")
                        NewGUID = newTcpConnClass.GUID
                      End Try
                    End If

                  Case TCP_ServerMessageType.Client_Debug_GetMessageServerConnections

                    Try
                      Dim DebugTcpConnClass As TcpConnClass
											Dim IPAddressString As String
											Dim ReturnString As String

                      For Each DebugTcpConnClass In ConnColl
                        Try
                          ReturnString = ""
													IPAddressString = "<unknown>"

													If (DebugTcpConnClass.ClientIPAddress IsNot Nothing) Then
														IPAddressString = DebugTcpConnClass.ClientIPAddress.ToString
													End If

													If (DebugTcpConnClass IsNot Nothing) Then
                            ReturnString = DebugTcpConnClass.ConnectionID.ToString & ", Active : " & DebugTcpConnClass.ActiveConnection.ToString & ", Type : " & DebugTcpConnClass.ApplicationType.ToString & ", Instance : `" & DebugTcpConnClass.InstanceName & "`, " & DebugTcpConnClass.RequestConnectString.ToString & ", Address : " & IPAddressString & ", WakeUpTime : " & DebugTcpConnClass.WakeupTime.ToString("dd MMM yyyy HH:mm:ss") & ", GUID : " & DebugTcpConnClass.GUID.ToString

                            Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_DebugMessage, FCP_Application.None, MessageHeader.InstanceName, ReturnString)

													End If

                        Catch ex As Exception
                        End Try
                      Next

                    Catch ex As Exception
                    End Try

                  Case TCP_ServerMessageType.Client_ApplicationUpdateMessage
                    ' ****************************************
                    ' UPDATE MESSAGES.
                    '
                    ' MessageHeader.Application     : Application Identifier for the Update Message
                    ' MessageHeader.MessageString   : Application specific Update message to be propogated.

                    ' This Message type is designed to handle Application Update Messages from  all
                    ' Application types.
                    '
                    ' The MessageHeader Application variable determines to what other connections this message
                    ' is propagated.
                    ' ****************************************
                    Call Process_UpdateMessage(Me, MessageHeader, newTcpConnClass, ConnColl)


                  Case TCP_ServerMessageType.Client_VeniceDataRequest
                    ' ****************************************
                    ' BBG Data Server Request
                    ' ****************************************

                    Call Process_GenericDataServerRequest(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_VeniceDataAnswer, TCP_ServerMessageType.Server_VeniceDataAnswer
                    ' ****************************************
                    ' BBG Data Server Answer
                    ' ****************************************

                    Call Process_GenericDataServerAnswer(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_NaplesDataRequest
                    ' ****************************************
                    ' BBG Data Server Request
                    ' ****************************************

                    Call Process_GenericDataServerRequest(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_NaplesDataAnswer, TCP_ServerMessageType.Server_NaplesDataAnswer
                    ' ****************************************
                    ' Naples Data Server Answer
                    ' ****************************************

                    Call Process_GenericDataServerAnswer(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_KansasStatusAnswer, TCP_ServerMessageType.Server_KansasStatusAnswer
                    ' ****************************************
                    ' Naples Data Server Answer
                    ' ****************************************

                    Call Process_GenericDataServerAnswer(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_KansasStatusMessage
                    ' ****************************************
                    ' Status message, treat like an update message and propogate.
                    '
                    ' MessageHeader.Application     : Application Identifier for the Update Message
                    ' MessageHeader.MessageString   : Application specific Update message to be propogated.
                    '
                    ' The MessageHeader Application variable determines to what other connections this message
                    ' is propagated.
                    ' ****************************************
                    Call Process_StatusMessage(Me, MessageHeader, newTcpConnClass, ConnColl)

                  Case TCP_ServerMessageType.Client_AcknowledgeOK
                    ' ****************************************
                    ' Acknowledge Message
                    ' ****************************************
                    ' Ignore

                  Case TCP_ServerMessageType.Client_Touchbase
                    ' ****************************************
                    ' TouchBase Message
                    ' ****************************************
                    ' Client requests TouchBase

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK)

                  Case TCP_ServerMessageType.Client_RequestConnect, TCP_ServerMessageType.Server_RequestConnect
                    ' ****************************************
                    ' RequestConnect
                    ' ****************************************

                    ' This connection has been polite, Mark it as OK.
                    ' We ignore Connections that do not 'RequestConnect' (they aren't nice !)

                    newTcpConnClass.ActiveConnection = True
										newTcpConnClass.RequestConnectString = MessageHeader.MessageString
                    newTcpConnClass.InstanceName = MessageHeader.InstanceName.ToUpper  ' Only pass messages between connections with the same instance Name.

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_AcknowledgeOK, FCP_Application.MessageServer, MessageHeader.InstanceName, "Connection Accepted")

                    ' Post Default Messages. Register Server Application and Set this connection GUID

                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.MessageServer, MessageHeader.InstanceName, "")
                    Call PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Client_SetGUID, FCP_Application.MessageServer, MessageHeader.InstanceName, ThisServerGUID.ToString)

                  Case TCP_ServerMessageType.Client_Disconnect, TCP_ServerMessageType.Server_Disconnect
                    ' ****************************************
                    ' Client Disconnect
                    ' ****************************************
                    ' Client requests Disconnect

                    Try
                      PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Client requested disconnect.")
                      newTcpConnClass.Client.Close()
                    Catch InnerEx As System.Net.Sockets.SocketException
                    Finally
                      newTcpConnClass.DeleteMe = True
                      ConnectionsToDelete.Add(newTcpConnClass)
                      ' ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
                    End Try

                  Case TCP_ServerMessageType.Client_MessageRejected, TCP_ServerMessageType.Server_MessageRejected
                    ' ****************************************
                    ' Message Rejected
                    ' ****************************************

                    Call PassMessageAlongReturnPath(Me, MessageHeader, newTcpConnClass)

                End Select

              Catch ex As System.Net.Sockets.SocketException
                Try
                  PostDebugMessage(Me, "Server SocketException (Connection Lost?) : " & ex.Message)
                  newTcpConnClass.Client.Close()
                Catch InnerEx As System.Net.Sockets.SocketException
                Finally
                  newTcpConnClass.DeleteMe = True
                  ConnectionsToDelete.Add(newTcpConnClass)
                  ' ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
                End Try

              Catch ex As Exception
                PostDebugMessage(Me, "Server Error : " & ex.Message & ", " & ex.StackTrace)
              End Try

            End If

          End While

          '
          ' Post TouchBase message to client if there has been no activity for the number of seconds indicated by
          ' TCPServerGlobals.FCP_CLIENT_WAKEUP_TIMER
          '
          ' Remove Connection if the TouchBase fails.
          '
          Try
            If (Not (newTcpConnClass Is Nothing)) AndAlso _
              (newTcpConnClass.DeleteMe = False) AndAlso _
              (newTcpConnClass.Stream.DataAvailable = False) AndAlso _
              (TCPServerGlobals.FCP_CLIENT_WAKEUP_TIMER > 0) AndAlso _
              (Now().Subtract(newTcpConnClass.WakeupTime).TotalSeconds >= TCPServerGlobals.FCP_CLIENT_WAKEUP_TIMER) Then

              newTcpConnClass.WakeupTime = Now()
              If (PostToMessageServer(newTcpConnClass, TCP_ServerMessageType.Server_Touchbase, FCP_Application.MessageServer, newTcpConnClass.InstanceName, "TouchBase") = False) Then
                PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Connection not responding. Removing Connection.")

                newTcpConnClass.DeleteMe = True
                ConnectionsToDelete.Add(newTcpConnClass)
                ' ConnColl.Remove(newTcpConnClass.ConnectionID.ToString)
              End If

            End If
          Catch ex As Exception
            If (Not (newTcpConnClass Is Nothing)) Then
              PostDebugMessage(Me, "(" & newTcpConnClass.ConnectionID.ToString & ") Error polling connection. Removing Connection.")

              newTcpConnClass.DeleteMe = True
              ConnectionsToDelete.Add(newTcpConnClass)
            End If
          End Try

        Catch Outer_For_ex As Exception
          PostDebugMessage(Me, "Server Error (Outer_For_ex): " & Outer_For_ex.Message)
        End Try

      Next ' For Each newTcpConnClass In ConnColl

      ' Remove Connections scheduled for Deletion
      If (ConnectionsToDelete.Count > 0) Then
        Dim DeleteIndex As Integer
        Dim thisConnection As TcpConnClass

        For DeleteIndex = 0 To (ConnectionsToDelete.Count - 1)
          thisConnection = CType(ConnectionsToDelete(DeleteIndex), TcpConnClass)

          Try
            If (thisConnection.Stream IsNot Nothing) Then
              thisConnection.Stream.Close()
            End If
          Catch ex As Exception
          End Try

          Try
            PostDebugMessage(Me, "(" & thisConnection.ConnectionID.ToString & ") Removing connection from collection.")

            ConnColl.Remove(CType(ConnectionsToDelete(DeleteIndex), TcpConnClass).ConnectionID.ToString)
          Catch ex As Exception

          End Try
        Next

        ConnectionsToDelete.Clear()
      End If

      ' Pause the thread if nothing is happening.

      If (LoopWorkDone = False) Then
        Threading.Thread.Sleep(25)
      End If

    End While
  End Sub

End Class
